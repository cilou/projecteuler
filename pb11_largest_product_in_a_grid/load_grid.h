#ifndef LOAD_GRID_H_
#define LOAD_GRID_H_

unsigned int** getGridArray(short gridSize, char* fullFilePath);
unsigned int** allocateMemForGrid(short gridSize);
unsigned int getIntFromString(char* str);
void displayGrid(unsigned int** grid, short gridSize);
void freeGrid(unsigned int** grid, short gridSize);

#endif // LOAD_GRID_H_
