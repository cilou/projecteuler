#include "load_grid.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned int** getGridArray(short gridSize, char* fullFilePath)
{
    if(gridSize <= 0) return NULL;

    FILE *fp = NULL;
    if((fp = fopen(fullFilePath, "r")) == NULL)
    {
        printf("The file %s cannot be open.\n", fullFilePath);
        exit(EXIT_FAILURE);
    }

    char ch = '\0', numberString[10 + 1] = {'\0'};
    unsigned int** grid = allocateMemForGrid(gridSize);
    unsigned short indexCol = 0, indexRow = 0;
    while(indexCol < gridSize && indexRow < gridSize)
    {
        ch = getc(fp);
        if(ch == ' ' || ch == '\n' || ch == '\r' || ch == EOF)
        {
            grid[indexRow][indexCol] = getIntFromString(numberString);
            if(ch != EOF)
            {
                if(ch == '\n' || ch == '\r') indexRow++, indexCol = 0;
                else indexCol++;
                unsigned short numberStringLength = strlen(numberString);
                for(int i = 0; i < numberStringLength; i++) numberString[i] = '\0';
            }
            else indexCol = indexRow = gridSize;
        }
        else numberString[strlen(numberString)] = ch;
    }

    fclose(fp);
    fp = NULL;

    return grid;
}

unsigned int** allocateMemForGrid(short gridSize)
{
    if(gridSize <= 0) return NULL;

    unsigned int** grid = (unsigned int**) malloc(gridSize * sizeof(int*));
    if(grid == NULL)
    {
        printf("Could not allocate memory for grid first level\n");
        exit(EXIT_FAILURE);
    }
    for(unsigned short i = 0; i < gridSize; i++)
    {
        grid[i] = (unsigned int*) calloc(gridSize, sizeof(int));
        if(grid[i] == NULL)
        {
            printf("Could not allocate memory for grid level [%d]\n");
            for(unsigned short j = 0; j < i; j++)
            {
                free(grid[j]);
                grid[j] = NULL;
            }
            free(grid);
            exit(EXIT_FAILURE);
        }
    }

    return grid;
}

unsigned int getIntFromString(char* str)
{
    if(str == NULL) return 0;

    unsigned int strLength = strlen(str), result = 0;
    unsigned short multiplier = 1;

    for(int i = strLength - 1; i >= 0; i--)
    {
        result += ((int)str[i] - 48) * multiplier;
        multiplier *= 10;
    }

    return result;
}

void displayGrid(unsigned int** grid, short gridSize)
{
    if(gridSize <= 0 || grid == NULL) return;

    for(int i = 0; i < gridSize; i++)
    {
        for(int j = 0; j < gridSize; j++)
            printf("%4d ", grid[i][j]);
        printf("\n");
    }
}

void freeGrid(unsigned int** grid, short gridSize)
{
    if(gridSize <=0 || grid == NULL) return;
    for(int i = 0; i < gridSize; i++)
    {
        for(int j = 0; j < gridSize; j++)
        {
            if(grid[j] != NULL)
            {
                free(grid[j]);
                grid[j] = NULL;
            }
        }
    }
    free(grid);
    grid = NULL;
}
