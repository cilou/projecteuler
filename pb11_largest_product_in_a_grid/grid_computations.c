#include "grid_computations.h"
#include <stdio.h>
#include <stdlib.h>

unsigned int computeLargestProductInGrid(unsigned int **grid,
                                         short gridSize,
                                         short consecutiveNumbers)
{
    if(grid == NULL) return 0;
    if(gridSize <= 0 || consecutiveNumbers <= 0) return 0;
    if(consecutiveNumbers > gridSize) return 0;

    unsigned short currentColumnPositionIndex = 0;
    unsigned int product = 0, highestProduct = 0;

    for(int i = 0; i < gridSize; i++)
    {
        currentColumnPositionIndex = 0;
        for(int j = currentColumnPositionIndex; j <= gridSize - consecutiveNumbers; j++)
        {
            product = computeUpRightProduct(grid, gridSize, i,
                                            currentColumnPositionIndex,
                                            consecutiveNumbers);
            if(product > highestProduct) highestProduct = product;

            product = computeRightProduct(grid, gridSize, i,
                                          currentColumnPositionIndex,
                                          consecutiveNumbers);
            if(product > highestProduct) highestProduct = product;

            product = computeDownRightProduct(grid, gridSize, i,
                                              currentColumnPositionIndex,
                                              consecutiveNumbers);
            if(product > highestProduct) highestProduct = product;

            currentColumnPositionIndex++;
        }

        currentColumnPositionIndex = 0;
        for(int j = currentColumnPositionIndex; j < gridSize; j++)
        {
            product = computeDownProduct(grid, gridSize, i,
                                     currentColumnPositionIndex,
                                     consecutiveNumbers);
            if(product > highestProduct) highestProduct = product;

            currentColumnPositionIndex++;
        }
    }

    return highestProduct;
}

unsigned int computeUpRightProduct(unsigned int** grid, short gridSize,
                                   short rowIndex, short columnIndex,
                                   short consecutiveNumbers)
{
    if(!isParametersValid(grid, gridSize, rowIndex, columnIndex, consecutiveNumbers))
        return 0;

    if(rowIndex+1 - consecutiveNumbers < 0) return 0;
    if(columnIndex + consecutiveNumbers > gridSize) return 0;

    unsigned int product = 1;
    unsigned short currentRow = rowIndex;
    for(int j = columnIndex; j < columnIndex + consecutiveNumbers; j++, currentRow--)
    {
        if(grid[currentRow][j] == 0)
        {
            product = 0;
            break;
        }
        else product *= grid[currentRow][j];
    }

    return product;
}

unsigned int computeRightProduct(unsigned int** grid, short gridSize,
                                   short rowIndex, short columnIndex,
                                   short consecutiveNumbers)
{
    if(!isParametersValid(grid, gridSize, rowIndex, columnIndex, consecutiveNumbers))
        return 0;

    unsigned int product = 1;
    for(int j = columnIndex; j < columnIndex + consecutiveNumbers; j++)
    {
        if(grid[rowIndex][j] == 0)
        {
            product = 0;
            break;
        }
        else product *= grid[rowIndex][j];
    }

    return product;
}

unsigned int computeDownRightProduct(unsigned int** grid, short gridSize,
                                   short rowIndex, short columnIndex,
                                   short consecutiveNumbers)
{
    if(!isParametersValid(grid, gridSize, rowIndex, columnIndex, consecutiveNumbers))
        return 0;

    if(rowIndex + consecutiveNumbers > gridSize) return 0;
    if(columnIndex + consecutiveNumbers > gridSize) return 0;

    unsigned int product = 1;
    unsigned short currentRow = rowIndex;
    for(int j = columnIndex; j < columnIndex + consecutiveNumbers; j++, currentRow++)
    {
        if(grid[currentRow][j] == 0)
        {
            product = 0;
            break;
        }
        else product *= grid[currentRow][j];
    }

    return product;
}

unsigned int computeDownProduct(unsigned int** grid, short gridSize,
                                short rowIndex, short columnIndex,
                                short consecutiveNumbers)
{
    if(!isParametersValid(grid, gridSize, rowIndex, columnIndex, consecutiveNumbers))
        return 0;

    if(rowIndex + consecutiveNumbers > gridSize) return 0;
    if(columnIndex >= gridSize) return 0;

    unsigned int product = 1;
    for(int currentRow = rowIndex; currentRow < rowIndex + consecutiveNumbers; currentRow++)
    {
        if(grid[currentRow][columnIndex] == 0)
        {
            product = 0;
            break;
        }
        else product *= grid[currentRow][columnIndex];
    }

    return product;
}

unsigned short isParametersValid(unsigned int** grid, short gridSize,
                                   short rowIndex, short columnIndex,
                                   short consecutiveNumbers)
{
    if(grid == NULL) return 0;
    if(gridSize <= 0) return 0;
    if(rowIndex < 0 || columnIndex < 0) return 0;
    if(rowIndex > gridSize || columnIndex > gridSize) return 0;
    if(consecutiveNumbers <= 0 || consecutiveNumbers > gridSize) return 0;

    return 1;
}
