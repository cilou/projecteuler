/*
    What is the greatest product of four adjacent numbers in the same direction
    (up, down, left, right, or diagonally) in the 20�20 grid?
*/

#include <stdio.h>
#include <stdlib.h>
#include "load_grid.h"
#include "grid_computations.h"
#include <assert.h>

void tests()
{
    unsigned short gridSize = 0, consecutiveNumbers = 0;

    gridSize = 4, consecutiveNumbers = 3;
    unsigned int** grid = NULL;

    grid = getGridArray(gridSize, "gridtest_4x4.txt");
    assert(computeLargestProductInGrid(grid, gridSize, consecutiveNumbers) == 2);
    freeGrid(grid, gridSize);

    gridSize = 6, consecutiveNumbers = 4;
    grid = getGridArray(gridSize, "gridtest_6x6.txt");
    assert(computeLargestProductInGrid(grid, gridSize, consecutiveNumbers) == 12);
    freeGrid(grid, gridSize);

    gridSize = 8, consecutiveNumbers = 4;
    grid = getGridArray(gridSize, "gridtest_8x8.txt");
    assert(computeLargestProductInGrid(grid, gridSize, consecutiveNumbers) == 2401);
    freeGrid(grid, gridSize);

    gridSize = 0, consecutiveNumbers = 4;
    grid = getGridArray(gridSize, "gridtest_6x6.txt");
    assert(computeLargestProductInGrid(grid, gridSize, consecutiveNumbers) == 0);
    freeGrid(grid, gridSize);

    gridSize = 6, consecutiveNumbers = 0;
    grid = getGridArray(gridSize, "gridtest_6x6.txt");
    assert(computeLargestProductInGrid(grid, gridSize, consecutiveNumbers) == 0);
    freeGrid(grid, gridSize);

    gridSize = 6, consecutiveNumbers = 8;
    grid = getGridArray(gridSize, "gridtest_6x6.txt");
    assert(computeLargestProductInGrid(grid, gridSize, consecutiveNumbers) == 0);
    freeGrid(grid, gridSize);

    gridSize = -6, consecutiveNumbers = 4;
    grid = getGridArray(gridSize, "gridtest_6x6.txt");
    assert(computeLargestProductInGrid(grid, gridSize, consecutiveNumbers) == 0);
    freeGrid(grid, gridSize);

    gridSize = 6, consecutiveNumbers = -4;
    grid = getGridArray(gridSize, "gridtest_6x6.txt");
    assert(computeLargestProductInGrid(grid, gridSize, consecutiveNumbers) == 0);
    freeGrid(grid, gridSize);

    gridSize = 0, consecutiveNumbers = 0;
    grid = NULL;
    assert(computeLargestProductInGrid(grid, gridSize, consecutiveNumbers) == 0);
    freeGrid(grid, gridSize);
}

int main()
{
    tests();

    const short gridSize = 20, consecutiveNumbers = 4;
    unsigned int** grid = getGridArray(gridSize, "grid.txt");
    int result = computeLargestProductInGrid(grid, gridSize,consecutiveNumbers);
    printf("Largest product: %d\n", result);

    //displayGrid(grid, gridSize);
    freeGrid(grid, gridSize);
    return 0;
}




