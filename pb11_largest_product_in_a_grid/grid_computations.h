#ifndef GRID_COMPUTATIONS_H_
#define GRID_COMPUTATIONS_H_

unsigned int computeLargestProductInGrid(unsigned int **grid,
                                         short gridSize,
                                         short consecutiveNumbers);
unsigned int computeUpRightProduct(unsigned int** grid, short gridSize,
                                   short rowIndex, short columnIndex,
                                   short consecutiveNumbers);
unsigned int computeRightProduct(unsigned int** grid, short gridSize,
                                   short rowIndex, short columnIndex,
                                   short consecutiveNumbers);
unsigned int computeDownRightProduct(unsigned int** grid, short gridSize,
                                   short rowIndex, short columnIndex,
                                   short consecutiveNumbers);
unsigned int computeDownProduct(unsigned int** grid, short gridSize,
                                short rowIndex, short columnIndex,
                                short consecutiveNumbers);
unsigned short isParametersValid(unsigned int** grid, short gridSize,
                                   short rowIndex, short columnIndex,
                                   short consecutiveNumbers);

#endif // GRID_COMPUTATIONS_H_
